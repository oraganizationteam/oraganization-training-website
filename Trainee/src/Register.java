 

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Servlet implementation class Register
 */
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      try{
    	  
      Connection conn=Dbconnection.getConnection();
		String username= request.getParameter("name");
		String address= request.getParameter("add");
		String email= request.getParameter("email");
		String password= request.getParameter("pass");
		
		request.setAttribute("user", username);
		
		PreparedStatement ps=conn.prepareStatement("insert into reg123 values(?,?,?,?)");
		ps.setString(1, username);
		ps.setString(2, address);
		ps.setString(3, email);
		ps.setString(4, password);
		
		int i=ps.executeUpdate();
		if(i>0){
			request.getRequestDispatcher("welcome.jsp").forward(request, response);
		}
		else {
			response.sendRedirect("error.jsp");
		}
      }
      catch(Exception e){
    	  e.printStackTrace();
      }
      }
	}


	
	