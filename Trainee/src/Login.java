

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			
			Connection conn=Dbconnection.getConnection();
		String uname =request.getParameter("uname");
		String password =request.getParameter("pass");
		request.setAttribute("user", uname);
		PreparedStatement ps= conn.prepareStatement("select * from reg123 where username=? and password=?");
	    ps.setString(1,uname);
	    ps.setString(2,password);
	    ResultSet rs=ps.executeQuery();
	    
	    if(rs.next()){
	    	request.getRequestDispatcher("Operation.jsp").forward(request, response);
	    }
	    else {
	    	response.sendRedirect("error.jsp");
	    }
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}